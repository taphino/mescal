!    This file is part of MESCal.
!    Copyright 2013-2015 Gabriele D'Avino
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.

program mescal
  use post_sc
  use microelectrostatic
#ifdef MPI  
  use module_mpi
#endif

  implicit none 
  logical       :: inp_exist,found
  character(50) :: input_fname,foo
  character(50) :: key,job_type,fname_out
  integer       :: strlen

#ifdef MPI
  call initial_mpi()
  if (proc_id==0) then
#endif

call start_banner
call cpu_time(time_begin)


call getarg(1,input_fname)
inquire(file=input_fname,exist=inp_exist)

if (inp_exist) then
   write(*,*) 'Reading input from: ',input_fname
   write(*,*) 
   call read_input(input_fname)
else
   write(*,*) 'ERROR: input file not found!'
   call stop_exec 
endif

strlen=len(trim(input_fname))
root_fname=adjustl(input_fname)
root_fname=root_fname(1:strlen-4)


! Determine the type of calulation to perform:
key="JOBTYPE"
call parse_str(key,job_type,found) 
if (found) then 
   write(*,"(1x,a20,a2,a4)") key,': ',job_type
   write(*,*)
else
   write(*,*) 'ERROR: Mandatory ',adjustl(trim(key)),' input not found! ' 
   call stop_exec 
endif


! tell mescal how to read coordinates
key="PDX"
call parse_log(key,use_pdx,found) 
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',use_pdx
   write(*,*) 'INFO: atomic coordinates read in pdx format'
   write(*,*)
endif

#ifdef MPI
  endif
  !brodcasting job_type
  call mpi_bcast(job_type          ,50,  MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)
  call show_mpi()
#endif


! SELF-CONSISTENT CALCULATIONS

if ( (job_type=='CR') .or. (job_type=='ME') ) then
   ! go for self consistent ME or CR calculation
   call driver_MECR 

elseif (job_type=='AAP') then
   call opt_calc_aap  

elseif (job_type=='VF_PBC') then
   call opt_VF_pc_cutoff


else
   write(*,*) '  ERROR: ', adjustl(trim(key)),' value ',adjustl(trim(job_type)),' is not contemplated!'
   call stop_exec

endif 

#ifdef MPI
  if (proc_id==0) then
#endif

! POST-SC TASKS 

key="VF_POINTS"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_VF_at_r
endif




key="OUT_CHG_DIP"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_chg_dip
endif

key="OUT_CHG_DIP_MM"
call parse_str(key,foo,found) 
if (found) then 
   call opt_write_chg_dip_mm
endif



! 2clear: now restarts are damped at every iteration (if required)
! OTHER TASKS
!key="OUT_RST"
!call parse_str(key,fname_out,found) 
!if (found) then
!   write(*,*) key,': ',fname_out
!   write(*,*)
!   call write_q_mu_V_F(fname_out)
!endif




! 2clear
! 26/10/15 big reorganization
!option=1000
!do while (option.ne.999)
!
!     select case (option) 
!
!
!!!! CR ROUTINES
!
!     case(1)
!        call opt_CR
!        option=1000
!
!     case(11)
!        call opt_CR_Fext
!        option=1000
!
!     case(12)
!        call opt_CR_Fext_PCB2D
!        option=1000 
!
!     case(13)
!        call opt_CR_Fext_PCB3D
!        option=1000
!
!     case(14)
!        read_ext_VF_pc=.true.
!        call opt_CR
!        option=1000
!
!
!!!!!   ME ROUTINES
!
!     case(2)
!        call opt_ME
!        option=1000
!
!     case(21)
!        call opt_ME_Fext
!        option=1000
!
!     case(22)
!        call opt_ME_Fext_PCB2D
!        option=1000
!
!     case(23)
!        call opt_ME_Fext_PCB3D
!        option=1000
!
!     case(24)
!        read_ext_VF_pc=.true.
!        call opt_ME
!        option=1000
!
!
!!     case(??)
!!        call opt_ME_rst
!!        option=1000
!
!
!!!!!  I/O ROUTINES
!
!
!     case(100)
!        call opt_write_dip_sel
!        option=1000
!
!     case(101)
!        call opt_write_qmVF
!        option=1000
!
!     case(102)
!        call opt_write_VF_at_r
!        option=1000
!
!     case(103)
!        call opt_write_dip_2q
!        option=1000
!
!
!!     case(???)
!!        call opt_ME_write_rst
!!        option=1000
!
!
!
!!!!!  OTHER ROUTINES
!
!
!     case(200)
!        call opt_calc_aap  
!        option=1000
!
!     case(201)
!        call opt_scale_apha_at
!        option=1000
!
!     case(202)
!        call opt_r_screen
!        option=1000
!
!     case(203)
!        call opt_VF_pc_cutoff
!        option=1000
!
!
!
!
!!!!!! REITERATE QUESTION
!
!     case(1000)
!        call ask_option
!
!
!     case default 
!        
!        write(*,*) '  ERROR: this input value is not contemplated!!! '
!        call stop_exec
!
!     end select
!
!end do

call stop_banner

#ifdef MPI
  endif
  call finalize_mpi()
#endif


! 2clear
!contains
!
!subroutine ask_option
!  implicit none
!
!write(*,"(80a)") ' '
!write(*,"(80a)") '  What should I do, Sir? (cit.)'
!write(*,"(80a)") ' '
!write(*,"(80a)") ' '
!write(*,"(80a)") ' Self-consistent Charge Redistribution calculation:'
!write(*,"(80a)") ' '
!write(*,"(80a)") '     1. Standard CR calculation'
!write(*,"(80a)") '    11. CR in uniform electric field'
!write(*,"(80a)") '    12. CR in uniform electric field with 2D PBC and cutoff (disk)'
!write(*,"(80a)") '    13. CR in uniform electric field with 3D PBC and cutoff (sphere)'
!write(*,"(80a)") '    14. CR calculation with F/V of permanent charges read from file'
!write(*,"(80a)") ' '
!write(*,"(80a)") ' Self-consistent charge MicroElectrostatic calculation:'
!write(*,"(80a)") '     2. Standard ME calculation'
!write(*,"(80a)") '    21. ME in uniform electric field'
!write(*,"(80a)") '    22. ME in uniform electric field with 2D PBC and cutoff (disk)' 
!write(*,"(80a)") '    23. ME in uniform electric field with 3D PBC and cutoff (sphere)'
!write(*,"(80a)") '    24. ME calculation with F/V of permanent charges read from file'
!write(*,"(80a)") ' '
!write(*,"(80a)") ' Input-output:'
!write(*,"(80a)") '   100. Dipole of a selection of molecules'
!write(*,"(80a)") '   101. Write charges, dipoles, fields and potentials at each atom (restart)'
!write(*,"(80a)") '   102. Field and potential at selected positions'
!write(*,"(80a)") '   103. Write induced dipoles as pairs of point charges'
!write(*,"(80a)") ' '
!write(*,"(80a)") ' Other routines (use with care!):'
!write(*,"(80a)") '   200. Calculate AAP and ZINDO charge-only polarizability'
!write(*,"(80a)") '   201. SC rescaling of atomic polarizabilities (for intramolecular interactions)'
!write(*,"(80a)") '   202. Electrostatic calculation with r-dependent screening'
!write(*,"(80a)") '   203. Compute potential and field of charges using 3D-PBC and cutoff'
!write(*,"(80a)") ' '
!write(*,"(80a)") ' 999. EXIT the program'
!write(*,"(80a)") ' '
!
!
!read(*,*)  option
!write(*,"(80a)") ' '
!
!end subroutine ask_option

end program mescal





