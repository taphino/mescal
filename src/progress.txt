     1. Standard CR calculation
    11. CR in uniform electric field
    12. CR in uniform electric field with 2D PBC and cutoff (disk)
    13. CR in uniform electric field with 3D PBC and cutoff (sphere)
    14. CR calculation with F/V of permanent charges read from file
 
 Self-consistent charge MicroElectrostatic calculation:
     2. Standard ME calculation
    21. ME in uniform electric field
    22. ME in uniform electric field with 2D PBC and cutoff (disk)
    23. ME in uniform electric field with 3D PBC and cutoff (sphere)
    24. ME calculation with F/V of permanent charges read from file
 
 Input-output:
   100. Dipole of a selection of molecules
   101. Write charges, dipoles, fields and potentials at each atom (restart)
   102. Field and potential at selected positions
   103. Write induced dipoles as pairs of point charges
 
 Other routines (use with care!):
   200. Calculate AAP and ZINDO charge-only polarizability
   201. SC rescaling of atomic polarizabilities (for intramolecular interactions)
   202. Electrostatic calculation with r-dependent screening
   203. Compute potential and field of charges using 3D-PBC and cutoff
