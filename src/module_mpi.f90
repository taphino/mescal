module module_mpi
#ifdef MPI
  use mpi
  
  implicit none  

  integer :: ierr  
  integer :: proc_id, num_procs
  integer :: proc_id_source
  
contains  
  subroutine initial_mpi()
  
    implicit none
    
    call MPI_INIT (ierr)
    call MPI_COMM_RANK (MPI_COMM_WORLD, proc_id, ierr)  
    call MPI_COMM_SIZE (MPI_COMM_WORLD, num_procs, ierr) 


  end subroutine
  
  subroutine show_mpi()
  
    implicit none
    
    if (proc_id==0) then
       write(*,*)
       write(*,*) ' INFO: Parallel execution with MPI '
       write(*,*) '       Number of cores: ',num_procs      
       write(*,*)
    endif
  
  end subroutine
  

  subroutine finalize_mpi()
  
    implicit none
    
    call MPI_FINALIZE (ierr) 
  
  end subroutine
#endif
end module
