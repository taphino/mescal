!    This file is part of MESCal.
!    Copyright 2013-2015 Gabriele D'Avino
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module math
  ! Mathematic tools
  use types
  implicit none

contains



function uptriangle_2_mat3(upt)
! returns full 3x3 symemtric matrix from upper triangle format
  implicit none
  real(rk)    :: upt(6)
  real(rk)    :: uptriangle_2_mat3(3,3)

uptriangle_2_mat3(1,1)=upt(1)
uptriangle_2_mat3(1,2)=upt(2)
uptriangle_2_mat3(2,1)=upt(2)
uptriangle_2_mat3(1,3)=upt(3)
uptriangle_2_mat3(3,1)=upt(3)
uptriangle_2_mat3(2,2)=upt(4)
uptriangle_2_mat3(2,3)=upt(5)
uptriangle_2_mat3(3,2)=upt(5)
uptriangle_2_mat3(3,3)=upt(6)

end function uptriangle_2_mat3

function mat3_2_uptriangleup(mat)
! returns  upper triangle format from full 3x3 symemtric matrix
  implicit none
  real(rk)    :: mat(3,3)
  real(rk)    :: mat3_2_uptriangleup(6)

  mat3_2_uptriangleup(1)=mat(1,1)
  mat3_2_uptriangleup(2)=0.50_rk*(mat(1,2)+mat(2,1))
  mat3_2_uptriangleup(3)=0.50_rk*(mat(1,3)+mat(3,1))
  mat3_2_uptriangleup(4)=mat(2,2)
  mat3_2_uptriangleup(5)=0.50_rk*(mat(2,3)+mat(3,2))
  mat3_2_uptriangleup(6)=mat(3,3)

end function mat3_2_uptriangleup


subroutine find_rotU(U,O,P1,P2)
  implicit none

  real(rk),intent(in)    :: O(3),P2(3),P1(3)
  real(rk),intent(out)   :: U(3,3)
  
  real(rk)      :: v1(3),v2(3),u1(3),u2(3),u3(3)

  v1=P1-O
  u1=v1/Norm3(v1)

  v2=P2-O
  u3=VectorProd3(u1,v2)
  u3=u3/Norm3(u3)
  u2=VectorProd3(u3,u1)

  U(:,1)=u1
  U(:,2)=u2
  U(:,3)=u3
 end subroutine find_rotU


subroutine Diagonalize3(matrix,evalues,evectors)
  ! simply an interface to EISPACK's RS
  real(rk),intent(in)  :: matrix(3,3)
  real(rk),intent(out) :: evalues(3),evectors(3,3)
  real(rk)             :: w1(3),w2(3)
  integer :: error
    
call RS(3,3,matrix,evalues,1,evectors,w1,w2,error) 
if (error/=0) write(*,*) ' E/Orror in subroutine Diagonalize3: diagonalization failed!!!'

end subroutine Diagonalize3


SUBROUTINE InvMat(matrix, inverse, n, errorflag)
!Subroutine to find the inverse of a square matrix
!Author : Louisda16th a.k.a Ashwith J. Rego
!Reference : Algorithm has been well explained in:
!http://math.uww.edu/~mcfarlat/inverse.htm           
!http://www.tutor.ms.unimelb.edu.au/matrix/matrix_inverse.html
	IMPLICIT NONE
	!Declarations
	INTEGER, INTENT(IN) :: n
	INTEGER, INTENT(OUT) :: errorflag  
                      !Return error status. -1 for error, 0 for normal
	REAL(rk), INTENT(IN), DIMENSION(n,n) :: matrix  !Input matrix
	REAL(rk), INTENT(OUT), DIMENSION(n,n) :: inverse !Inverted matrix
	
	LOGICAL :: FLAG = .TRUE.
	INTEGER :: i, j, k, l
	REAL(rk) :: m
	REAL(rk), DIMENSION(n,2*n) :: augmatrix !augmented matrix
	
	!Augment input matrix with an identity matrix
	DO i = 1, n
		DO j = 1, 2*n
			IF (j <= n ) THEN
				augmatrix(i,j) = matrix(i,j)
			ELSE IF ((i+n) == j) THEN
				augmatrix(i,j) = 1
			Else
				augmatrix(i,j) = 0
			ENDIF
		END DO
	END DO
	
	!Reduce augmented matrix to upper traingular form
	DO k =1, n-1
		IF (augmatrix(k,k) == 0) THEN
			FLAG = .FALSE.
			DO i = k+1, n
				IF (augmatrix(i,k) /= 0) THEN
					DO j = 1,2*n
						augmatrix(k,j) = augmatrix(k,j)+augmatrix(i,j)
					END DO
					FLAG = .TRUE.
					EXIT
				ENDIF
				IF (FLAG .EQV. .FALSE.) THEN
					PRINT*, "Matrix is non - invertible"
					inverse = 0
					errorflag = -1
					return
				ENDIF
			END DO
		ENDIF
		DO j = k+1, n			
			m = augmatrix(j,k)/augmatrix(k,k)
			DO i = k, 2*n
				augmatrix(j,i) = augmatrix(j,i) - m*augmatrix(k,i)
			END DO
		END DO
	END DO
	
	!Test for invertibility
	DO i = 1, n
		IF (augmatrix(i,i) == 0) THEN
			PRINT*, "Matrix is non - invertible"
			inverse = 0
			errorflag = -1
			return
		ENDIF
	END DO
	
	!Make diagonal elements as 1
	DO i = 1 , n
		m = augmatrix(i,i)
		DO j = i , (2 * n)				
			   augmatrix(i,j) = (augmatrix(i,j) / m)
		END DO
	END DO
	
	!Reduced right side half of augmented matrix to identity matrix
	DO k = n-1, 1, -1
		DO i =1, k
		m = augmatrix(i,k+1)
			DO j = k, (2*n)
				augmatrix(i,j) = augmatrix(i,j) -augmatrix(k+1,j) * m
			END DO
		END DO
	END DO				
	
	!store answer
	DO i =1, n
		DO j = 1, n
			inverse(i,j) = augmatrix(i,j+n)
		END DO
	END DO
	errorflag = 0
END SUBROUTINE InvMat




function Norm3(r)
  implicit none
  real(rk)  :: r(3)
  real(rk)  :: Norm3

  Norm3=sqrt(r(1)**2+r(2)**2+r(3)**2)
end function Norm3


function VectorProd3(p1,p2) RESULT(p3)
  implicit none
  real(rk),dimension(3),intent(in):: p1,p2
  real(rk),dimension(3)           :: p3

  p3(1)=p1(2)*p2(3)-p1(3)*p2(2)
  p3(2)=p1(3)*p2(1)-p1(1)*p2(3)
  p3(3)=p1(1)*p2(2)-p1(2)*p2(1)
end function VectorProd3


function DotProd3(p1,p2) RESULT(dp)
  implicit none
  real(rk),dimension(3),intent(in)::p1,p2
  real(rk)   ::dp
  
  dp=p1(1)*p2(1)+p1(2)*p2(2)+p1(3)*p2(3)
end function DotProd3



end  module math
