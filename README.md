

   Welcome to

     /  \     /  |/        | /      \  /      \           /  |
     $$  \   /$$ |$$$$$$$$/ /$$$$$$  |/$$$$$$  |  ______  $$ |
     $$$  \ /$$$ |$$ |__    $$ \__$$/ $$ |  $$/  /      \ $$ |
     $$$$  /$$$$ |$$    |   $$      \ $$ |       $$$$$$  |$$ |
     $$ $$ $$/$$ |$$$$$/     $$$$$$  |$$ |   __  /    $$ |$$ |
     $$ |$$$/ $$ |$$ |_____ /  \__$$ |$$ \__/  |/$$$$$$$ |$$ |
     $$ | $/  $$ |$$       |$$    $$/ $$    $$/ $$    $$ |$$ |
     $$/      $$/ $$$$$$$$/  $$$$$$/   $$$$$$/   $$$$$$$/ $$/


  MESCal is a suite of routines for MicroElectroStatic Calculations
  in organic solids.

  If you are looking for a distilled alcoholic beverage made from
  agave americana you are in the wrong place, sorry!
  http://en.wikipedia.org/wiki/Mezcal


  Copyright by Gabriele D'Avino 2013-2015
  gabriele.davino@gmail.com

  MESCal is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License.

  MESCal is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MESCal.  If not, see <http://www.gnu.org/licenses/>.

  Publications of results obtained with MESCal should cite
  G. D'Avino et al., J. Chem. Theory Comput. 10, 4959 (2014)


  List of contributors:
       Jing Li          : MPI parallelization
