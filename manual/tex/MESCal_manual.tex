\documentclass[11pt,A4paper]{article}

\usepackage{graphicx} 
%\usepackage{palatino}
\usepackage{helvet}
\renewcommand*{\familydefault}{\sfdefault}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\usepackage{color}
\usepackage{amssymb,amsmath}


%\newcommand{\mescal}{ \begin{verbatim} MESCal \end{verbatim} }
\newcommand{\mescal}{MESCal}
%\newcommand{\mescal}{{\verb MESCal}}



%\newcommand{\muv}{ \boldsymbol{\mu} }

\linespread{1.2}

\title{MESCal user guide}


\begin{document}

\begin{titlepage}
\begin{center}

{\bfseries \Huge
MESCal user guide}\\
\vspace{1cm}

{ \Large
{\it \mescal\ is a suite of routines for MicroElectroStatic Calculations in organic solids}}\\
\vspace{2cm}
\includegraphics[width=0.8\textwidth]{mescal_logo.png}
%\includegraphics[width=10cm]{mescal2.png}
\vfill

{\large  Developed by Gabriele D'Avino \\
{\verb gabriele.davino@neel.cnrs.fr  } \\
\vspace{1cm}
Last update: \\
\today 
}


\end{center}
\end{titlepage}

\tableofcontents
\clearpage


\section{Introduction}
MESCal is a computer program for the evaluation of electrostatic and induction
interactions in molecular solids.
The code is an original and unified implementation of induced dipoles (microelectrostatic, ME)
\cite{Applequist72,Bounds_Munn_CP79,stone,silinsh_capek}
and intramolecular charge redistribution (CR) \cite{ts_CPL2001,ts_prb01} schemes.
The theoretical backgrounds of the methods have been published in Ref. \cite{davino_JCTC15}:
\vspace{0.2cm} \\
G. D’Avino, L. Muccioli, C. Zannoni, D. Beljonne, and  Z.~G. Soos, 
{\it Electronic polarization in organic crystals: A comparative study of induced dipoles
 and intramolecular charge redistribution schemes},
Journal of Chemical Theory and Computation 10, 4959 (2014).
\vspace{0.2cm} \\
Publications of results obtained with \mescal\  should cite this paper.





\subsection{Acknowledgments}

It is a pleasure to acknowledge the precious contributions from David Beljonne, 
Luca Muccioli and Zoltan G. Soos in the code design and development stage.
MPI parallelization has been realized by Jing Li.
The \mescal\ logo is a great artwork by Otello M. Roscioni.



\subsection{Copyrights and disclaimers}

\mescal\ (copyrights by Gabriele D'Avino) is a free software: 
you can redistribute it and/or modify              
it under the terms of the GNU General Public License as published by 
the Free Software Foundation, either version 3 of the License.       
\mescal\  is distributed in the hope that it will be useful,            
but WITHOUT ANY WARRANTY; without even the implied warranty of       
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
GNU General Public License for more details.
You should have received a copy of the GNU General Public License    
along with \mescal.  If not, see <http://www.gnu.org/licenses/>.          

You are strongly encouraged to report bugs and suggestions to the author:
{\verb gabriele.davino@neel.cnrs.fr }\,!




\subsection{Program overview}

\mescal\ computes electrostatic and induction interactions between non-overlapping molecular
fragments.
An atomic point charge picture is adopted for electrostatic interactions, while molecular 
polarization can be described either with ME and CR models.
All the details of the methods can be found  in Ref. \cite{davino_JCTC15}.

The basic program inputs are the atomistic structure of the molecular system under examination 
(pdb file) and the file(s) with molecular inputs.
\mescal\ works on finite size systems (supramolecular clusters) and can handle several thousands 
of molecules.
Periodic boundary conditions in 2D and 3D within a cutoff have been implemented for describing 
systems with translational symmetry and zero net charge. 


Figure \ref{f:sc} sketches the functioning of the program for a ME calculation - 
a conceptually identical scheme applies to CR. 
The core part of a \mescal\ run is the self-consistent solution of linear equations for the determination of molecular polarization.
This is achieved  with an iterative scheme (Jacobi over-relaxation) corresponding to
the part of the left chart within the feedback loop.
Iterations refining the solutions at the previous step are performed until the energy of the system
converges within a predefined tolerance threshold.
The program is suitable for both serial and parallel (MPI) execution.


\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth]{selfcons.png}
\caption{Left: Flowchart of a self-consistent ME calculation with \mescal.
The zero-th order value of induced dipoles is used to compute potential and fields at atomic sites and 
update dipoles. This process is repeated until convergence of the system energy.
Right: Energy of a spherical cluster of pentacene molecules at different iterations.
Convergence is achieved within about 30  iterations. }
\label{f:sc}
\end{figure}



\clearpage
%\section{Program functioning and inputs}

\section{The \mescal\ input file}

The \mescal\ input file is a text file consisting in a series of key-value pairs 
and input blocks of several lines.
The format should be rather intuitive, the following is a simple input for a ME calculation:
\begin{verbatim}
JOBTYPE  ME
PDB A3P_m1.pdb
INPUT_MOL
  A3P  A3_P_esp.mei
  A3Z  A3_Z_esp.mei
END
MAX_ITER 200
TOL_ENE 1e-5
\end{verbatim}
This input requires a ME calculation to be performed for the structure given in the  
(\texttt{A3P\_m1.pdb}) file.
The input block \texttt{INPUT\_MOL} specifies the parameter files for each chemical specie
present in the pdb file and is terminated by \texttt{END}.
The records \texttt{JOBTYPE}, \texttt{PDB} and \texttt{INPUT\_MOL} are mandatory for every  
calculation. 
\texttt{MAX\_ITER} and \texttt{TOL\_ENE} are instead optional and can be used to override
the default values.
Other input records allow for other functionalities.

\subsubsection{List of available input records}
\label{s:inputs}

\begin{itemize}
\renewcommand{\labelitemi}{\raisebox{1pt}{\tiny$\blacksquare$}} 
\item \texttt{JOBTYPE <string>}\\
  Type of calculation to be performed. Mandatory parameter.
  Accepted values: 
    \texttt{ME}: self-consistent ME calculation; 
    \texttt{CR}: self-consistent CR calculation; 
    \texttt{VF\_PBC}: calculation of potential and fields at atoms due to permanent charges, 
      possibly accounting for periodic replica within spherical cutoff; 
      \texttt{AAP}: compute INDO/S atom-atom polarizability tensor for CR calculation.
      This latter option requires the exectutable \texttt{rrzindo} (not distributed with
      MESCal) in your path 
      

\item \texttt{PDB <string>}\\
   Specify the pdb file name. Mandatory parameter. 
   Upon reading the pdb file, \mescal\ identifies the presence of one or more 
   \emph{chemical species} from the residue name record (3 characters). 
   Each chemical specie requires the specification of a specific parameter file(s) in the 
   \texttt{INPUT\_MOLPDB} block. 
   A given molecule and its ions are different chemical species.   
  
\item \texttt{PDX <logical>}\\
  Specify if the structure should be read in pdx format instead of pdb.
  A pdx file is the same of a pdb in the first 30 columns of each ATOM 
  record line,  followed by the values of the Cartesian coordinate in 
  free-format. This allows for higher precision of input coordinates, 
  if needed, or less rigid input.

\item \texttt{INPUT\_MOL}\\
  Specify the molecular parameter file(s) for each chemical specie in the system.
  Mandatory input block (unless for \texttt{JOBTYPE AAP} calculations)
  to be terminated with \texttt{END}.
  Each line within the block specifies the parameters for one of the chemical 
  species in the pdb file. 
  For ME calculations each line should include the name of the chemical specie followed 
  by the mei filename as in the example above.
  For CR calculations each line should include the name of the chemical specie followed 
  by the cri and aap filenames.
  See Section \ref{s:molinp} for information about molecular input files.


\item \texttt{TOL\_ENE <real>} (default $10^{-6}$)\\
  Required energy tolerance in eV.
  
\item \texttt{MAX\_ITER <integer>} (default: 100.0)\\
  Maximum number of iterations. 

\item \texttt{ENE\_MAX <real>} (default 1.0)\\
  Maximum energy per molecule allowed in eV.
  The program stops its execution if the absolute value of the maximum energy per molecule
  exceeds the specified value. 

\item \texttt{DAMP <real>}  (default 0.7)\\
  Damping factor in the iterative solution update.
  Accepted values are between  0 (no damping) and 1 (no update). 
  The damping determines the mixing of the solution of the linear equations 
  at a given step with that at the previous one (Jacobi over-relaxation). 
  Large dampings lead to safer convergence but require more iterations. 
  The default should be adequate for most calculations.
  
\item \texttt{SCREEN\_DIP\_INTRA <logical>} (default F)\\ 
  Allow for screened intramolecular dipole-dipole interactions in ME calculation. 
  The screening factor should be set with \texttt{SCREENING}.

\item \texttt{SCREEN\_DIP\_INTER <logical>} (default F)\\ 
  Allow for screened intermolecular dipole-dipole interactions in ME calculation. 
  The screening factor should be set with \texttt{SCREENING}.

\item \texttt{SCREENING <real>} (default 0.0)\\ 
  Screening parameter of dipole fields (Tinker-exponential 
  screening functions \cite{Ren_Ponder_JPCB03,davino_JCTC15}).

\item \texttt{OUT\_RST <string>} \\
  Write a file reporting the value of potential, field, 
  permanent/induced charges/dipoles at each atom.
  This text file can be used (also) for restart purpose. 

\item \texttt{RESTART <string>}\\
  Request self-consistent calculation to restart from a specified guess.


\item \texttt{FEXT\_UNI <real(3)>} \\
  Set a uniform electric field to the specified value (V/\AA\ units).

\item \texttt{FEXT <string>} \\
  Read non-uniform external electric potential and field from file. 
  The input file has one line per atom, each of which contains the value of the  
  potential (in V) and the 3 Cartesian components of the field (in V/\AA) in free format.

\item \texttt{PBC <integer>} \\
  Activate periodic boundary conditions (PBC).
  Accepted values: \texttt{2}: PBC active in 2D; \texttt{3}: PBC active in 3D.


\item \texttt{PBC\_BOX} \\
  Specify the simulation cell vectors. 
  Mandatory input block in calculations with periodic boundary conditions (see \texttt{PBC})
  to be terminated with \texttt{END}.
  Each line within the block specifies one of cell vectors (3 real numbers).
  Three cell vectors should be specified also in calculations with 2D PBC.


\item \texttt{PBC\_CUTOFF} <real> \\
  Set the cutoff distance for periodic replica in calculations with PBC. 

\item \texttt{PBC\_SLAB\_VEC} <integer> \\
  In calculations with 2D periodic boundary conditions defines the facet of the 
  slab by setting the direction (cell vector) that you don't want to replicate.
  Accepted values: 1,2,3.
  For example by specifying \texttt{PBC\_SLAB\_VEC 1}, the 2D slab will be
  generated by replicating the cell in the plane defined by the 2nd and 3rd 
  unit cell vectors.

\item \texttt{MIRROR <integer> <real>}\\
Perform ME/CR calculation accounting for image charges and dipoles. 
The mirror plane should be orthogonal to one of the Cartesian axes, the one 
specified by the first integer at the position specified by the second real number.
For example \texttt{MIRROR 2 7.5} indicates a mirror plane with equation
$y=7.5$.


\item \texttt{XYZ\_AAP <string>} \\
Name of the xyz file for atom-atom polarizability calculation. Mandatory in 
\texttt{JOBTYPE AAP} calculation.
The xyz file has the standard format except that the second line specifies the 
molecular charge and the spin multiplicity (two integers) and that the 
element symbol is replaced by the atomic number.
Restricted-open INDO/S SCF is assumed by default for open shell systems.
Unrestricted SCF  can be performed by providing negative spin multiplicity 
(the absolute value is used in the calculation), but this can be very inaccurate.   

\item \texttt{CLEAN\_AAP <logical>} (default T)\\ 
Clean input/output files created in individual INDO/S calculations for atom-atom
polarizability calculation. 

\item \texttt{SITE\_ENERGY\_ELSTAT <string>}\\
It specifies to compute electrostatic site energies in electrostatic sums
calculations with PBC and to write the result to the specified file. 
Relevant only to \texttt{JOBTYPE VF\_PBC} calculations, and requires the
input block \texttt{INPUT\_MOL\_ALT}.

\item \texttt{INPUT\_MOL\_ALT} \\
Input block formally equivalent to \texttt{INPUT\_MOL} that specifies an additional mei
file for each chemical specie with the atomic charges of molecular ions.
Mandatory if \texttt{SITE\_ENERGY\_ELSTAT} is set.

\item \texttt{ROUND\_CHARGE <logical>} (default T)\\ 
Integer charges for each chemical specie are expected by default.
The program rounds the net molecular charge to the nearest integer and 
stops the execution whether the rounding exceeds a small tolerance. 
Fractional charges can be used by disabling this rounding.  

\item \texttt{VF\_POINTS <string>} \\
Request the calculation of the electrostatic potential and field at the 
positions given in the associated file. 
Positions should be specified as three real numbers ($x$, $y$, $z$ coordinates),
one point per line. 
The file should not have blank lines at the end.

\item \texttt{OUT\_CHG\_DIP <string>} \\
Write permanent and induced charges and dipoles at the end of a calculation
to file with specified name.
Dipoles are written as pairs of point charges at 0.1 \AA\ distance.
%This routine is for QM/MM embedding of DFT calculations.

\item \texttt{OUT\_CHG\_DIP\_MM <string>} \\
  Same as \texttt{OUT\_CHG\_DIP}, except for the the output format.
  The latter is suitable for QM/MM calculations with ORCA.


  
  

  
\end{itemize}







\section{Molecular input files}
\label{s:molinp}

Molecular inputs are essentially the set of atomic charges and the molecular polarizability 
tensor.
Both quantities can be computed with any quantum chemistry code, typically at the B3LYP/6-311++G** 
or comparable level of theory.
We also recommend the use of charges from electrostatic potential fitting (e.g. ESP \cite{esp}). 
These inputs should be provided in different ways in ME and CR calculations.


Keep in mind that one has to provide one input file for each chemical specie present in the pdb file.
In building the input files one should care about the ordering of atoms that must be the same as
in the pdb file. 





\subsection{Microelectostatics input (mei) file}

In ME calculations the molecular inputs can be provided with different input formats,
suitable for:
\begin{itemize}
\item Rigid molecule (most commmon case)
\item Arbitrary molecule (free-input)
\item Isotropic polarizable point.
\end{itemize}
 


\subsubsection{Rigid-molecule mei file}

This input format is suitable for rigid molecules.
The rotation of the polarizability in the reference frame (the one according to which
is provided the pdb file) is performed with the same rotation
matrix for all the atoms of the molecule.
This input format is explained through the example of a naphthalene molecule,
see Figures \ref{f:a2} and \ref{f:mei_arp}.

%The mei file consists of 3 different parts. 
The first line contains the number of atoms (repeated two times) and a sequence of 3 characters.
The first two indicate that this input is atomistic \texttt{A} (polarizable points at atoms)
and that the molecule is rigid \texttt{R}.
The third character specifies how the molecular polarizable is distributed on the atoms.
At each atom is assigned a polarizability tensor $\alpha_i=f_i\alpha$, where $\alpha$ is the molecular
polarizability tensor; $f_i$ are the weighting coefficients such that $\sum_i f_i=1$.
In the example in Figure \ref{f:mei_arp} the character \texttt{P} specifies to distribute the 
polarizability proportionally to the static atomic polarizability of the corresponding element 
\cite{CRChandbook}.
Alternatively, one can use the number of valence electrons as weighting criterion with the character
\texttt{V}.

The following lines, one for each atom of the molecule, contain:
ones in the 1st and 4th columns (for historical reasons, other values are not admitted), 
a progressive index in the 2nd column, the value of atomic charges in the 3rd column, 
and the atomic number in the 5th column.

The penultimate line specifies the molecular polarizability tensor in \AA$^3$ in upper-triangle
form, i.e. $\alpha_{xx}$  $\alpha_{xy}$  $\alpha_{xz}$  $\alpha_{yy}$  $\alpha_{yz}$  $\alpha_{zz}$.
The last line provides 3 atomic indexes that defined the $xyz$  reference frame in which is
expressed  the polarizability tensor: 
\begin{eqnarray}
  \vec x&=& \frac{\vec r_2 - \vec r_1 }{|\vec r_2 - \vec r_1|} \\
  \vec z&=& \vec x \times \frac{\vec r_3 - \vec r_1 }{|\vec r_3 - \vec r_1|} \\
  \vec y&=& \vec z \times \vec x 
\end{eqnarray}
where $\vec r_i$, $i=1,2,3$, are the positions of the atom whose indexes are
provided in the last line.  
In the example of naphatlene the $\vec r_1$ is the position of atom 1, 
$\vec r_2$ is the position of atom 11, and $\vec r_3$ is the position of atom 5.
In practice, the first two atoms defines the $\vec x$, $\vec z$ is normal to the plain
of the three atoms, and $\vec y$ is orthogonal to the $xz$ plan, so that $xyz$ forms
a right-handed orthonormal  reference frame.  
For naphthalene $\vec x$ in the long molecular axis, $\vec y$ is the other in-plane axis,
and $\vec z$ is normal to the molecular plane. 


In practice, we recommend the following parameterization procedure:
\begin{enumerate}
\item Determine the three atoms that define  molecular orientation.
  It is convenient to opt for three atoms that define a frame consistent with the
  molecular symmetry, as in  the example of the  naphthalene molecule
  (Figures \ref{f:a2} and \ref{f:mei_arp}).
  In the chosen frame, the polarizability tensor is diagonal, consistent with D2h symmetry.
\item Rotate the molecule in the frame defined by the three atoms.
\item Perform the calculation of the polarizability for the orientation determined at
  the previous step. Pay attention that some quantum chemistry codes (e.g. Gaussian) rotate
  the molecule in the so called ``standard orientation''. This is a default option to be
  disabled ( \texttt{NoSymmetry} keyword in Gaussian).
  Atomic charges are independent on the orientation, but can be conveniently calculated
  in the same job.
\item Assemble the mei file, paying attention to the format and units.
\end{enumerate}



\clearpage


\begin{figure}[t]
\centering
\includegraphics[width=0.5\textwidth]{a2.png}
\caption{Naphthalene molecule. Atomic numbering must be consistent with the ordering of 
atoms in the pdb and mei (Figure \ref{f:mei_arp}) files.}
\label{f:a2}
\end{figure}

\begin{figure}[ht]
{ \small
\begin{verbatim}
18 18 A R P  
1    1      -0.115497    1    6
1    2      -0.288973    1    6
1    3       0.236284    1    6
1    4      -0.288973    1    6
1    5      -0.115497    1    6
1    6       0.130508    1    1
1    7       0.155820    1    1
1    8       0.155820    1    1
1    9       0.130508    1    1
1    10      0.236284    1    6
1    11     -0.115497    1    6
1    12     -0.288973    1    6
1    13     -0.288973    1    6
1    14     -0.115497    1    6
1    15      0.130508    1    1
1    16      0.155820    1    1
1    17      0.155820    1    1
1    18      0.130508    1    1
25.0485  0.0000  0.0000  18.0028  0.0000  9.2233
1  11  5 
\end{verbatim} }
\caption{Example mei file for rigid molecules relative to the (neutral) naphthalene molecule in Figure \ref{f:a2}.}  
\label{f:mei_arp}
\end{figure}


\clearpage
\subsubsection{Free-input mei file}

This input format covers the more general case in which a molecule cannot be considered as rigid,
allowing the possibility to specify a polarizability tensor for each atom.
This option provides the user additional flexibility in the definition of the molecular model,
which may be useful in some (rare) cases.

The example in Figure \ref{f:mei_af} refers to a poly-3-hexyl-thiophene (P3HT) decamer with implicit hydrogens 
(heavy atoms only). \cite{Davino_Mothy_JPCC13} 
The first line presents the number of atoms (repeated twice) and  the two 
characters \texttt{A} (atomistic) \texttt{F} (free-input format). 
The following lines, one per each atoms include:
ones in the 1st, 4th and 5th (real number) columns; 
a progressive index in the 2nd column;
the atomic charge in the 3rd column; 
columns 7-9 has the indexes of the atoms defining the orientation of the polarizability tensor
specific to that atom (the reference frame is defined as in the case of rigid molecule); 
columns 10-15 specify the polarizability tensor in \AA$^3$ and upper-triangle form.
In the example of P3HT in Figure \ref{f:mei_af}, the first 5 atoms are those of the thiophene ring
and they bear the same polarizability tensor.
The following 6 atoms are those of the alkyl chain and bear isotropic polarizabilities.


\begin{figure}[ht]
{ \small
\begin{verbatim}
110 110  A  F 
1   1   0.185414  1  1.0  5 4 2   6.3862 0.0 0.0 1.7722 0.0 0.8035
1   2  -0.074913  1  1.0  5 4 2   6.3862 0.0 0.0 1.7722 0.0 0.8035
1   3  -0.119732  1  1.0  5 4 2   6.3862 0.0 0.0 1.7722 0.0 0.8035
1   4  -0.174051  1  1.0  5 4 2   6.3862 0.0 0.0 1.7722 0.0 0.8035
1   5  -0.090044  1  1.0  5 4 2   6.3862 0.0 0.0 1.7722 0.0 0.8035
1   6   0.201725  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1   7   0.107954  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1   8  -0.060681  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1   9   0.000000  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1  10   0.000000  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1  11   0.000000  1  1.0  1 2 3   1.8140 0.0 0.0 1.8140 0.0 1.8140
...                                                         
1 109   0.000000  1  1.0  1 2 3   1.7220 0.0 0.0 1.7220 0.0 1.7220
1 110   0.000000  1  1.0  1 2 3   1.8140 0.0 0.0 1.8140 0.0 1.8140
\end{verbatim} }
\caption{Example of mei file with free-format input for neutral P3HT decamer.}  
\label{f:mei_af}
\end{figure}



\clearpage
\subsubsection{One-site molecule mei file}

It is also possible to define isotropic and anisotropic polarizable points as one-atom molecules. 
This allows calculations on atomic lattices, or a coarse graining of molecular atomistic structure. 

The mei file for a dummy atom with zero charge and isotropic polarizability of 10 \AA$^3$: 
\begin{verbatim}
1 1 I I
1 1     0.00    10.00
\end{verbatim}

The following example is for a dummy atom with zero charge and tensor polarizability:
\begin{verbatim}
1 1 T T
1 1     0.00     33.84  -0.20  -1.24  30.20  -1.78  51.93
\end{verbatim}
where the site polarizability tensor is given in upper-triangle format, in
the laboratory Cartesian reference frame. 

One-site molecules are considered as dummy atoms  within the \mescal. 
Dummy atoms of different type (residue name, charge, polarizability) can be used in ME calculations.
Dummy atoms are also compatible with CR calculations, but with the following restrictions:
(i) they have to be named DMY in the pdb file (residue name);
(ii) The site charge is set to zero, other input values in the mei file are ignored;
(iii) In the \texttt{INPUT\_MOL} block you only pass the mei file for DMY, the aap file 
does not apply to dummy atoms and is not needed.



\subsection{Charge redistribution input (cri) and atom-atom polarizability (aap) files}

In CR calculations two files are required for each chemical specie, namely 
the charge redistribution input (cri) file and the atom-atom polarizability (aap) file.
The cri file presents exactly the same format as the mei file for rigid molecules.
Molecular polarization is described differently than in ME, as it includes a main 
contribution from charge flow (aap file, see below), and a small correction from induced dipoles.
The penultimate line of the cri file hence corresponds to the polarizability  of classical 
polarizable points, and in the case of naphthalene it mainly consist in an off-plane 
contribution.



The aap file contains the symmetric atom-atom polarizability tensor 
\begin{equation}
\Pi_{ij}=\frac{\partial q_i}{\partial \phi_j}
\end{equation}
where $q_i$ is the charge at atom $i$ and $\phi_j$ is the potential at atom $j$.
$\Pi$ can be computed at the INDO/S level \cite{ts_CPL2001,ts_prb01} in 
\texttt{JOBYPE AAP} calculations (see Section \ref{s:inputs}). 
Figure \ref{f:aap} provides an example of the aap file format.

\begin{figure}[ht]
{ \small
\begin{verbatim}
 1    1    1.977452799613
 1    2   -0.745658066159
 1    3    0.031989048188
...
 1   17   -0.007642960420
 1   18   -0.012717015377
 2    1   -0.745658066159
 2    2    2.009401756981
 2    3   -0.523413818661
 2    4   -0.006308890852
...
18   17   -0.026792440860
18   18    0.637197944902
\end{verbatim} }
\caption{Example of atom-atom polarizability (aap) file for neutral naphthalene.
The tensor is given in the $i\  j\  \Pi_{ij}$ format with $\Pi_{ij}$ in \AA$^{-1}$.}  
\label{f:aap}
\end{figure}


\clearpage
\section{Compiling \mescal}

MESCal source codes can be found in the \texttt{src} directory.
Compilation scripts for the serial (\texttt{compile\_serial.sh}) and
parallel (\texttt{compile\_mpi.sh}) versions are provided therein.
The respective Makefiles can be edited to select other compilers or
to modify specific settings. 


\section{Running \mescal}

The serial version of mescal can be executed as follows:
\begin{verbatim}
 $PATH_TO_src/mescal neu.inp > neu.out
\end{verbatim}

Parallel execution (on 8 cores in the example below) can be run as:
\begin{verbatim}
 mpirun -np 8  $PATH_TO_src/mescal_mpi neu.inp > neu.out
\end{verbatim}


\clearpage

\section{Calculation of polarization energies}

The so-called polarization energies are defined as:
\begin{eqnarray}
  \Delta^+&=& IP_{gas} - IP_{cry}       \\
  \Delta^-&=& EA_{cry} - EA_{gas}.
\label{e:PolEne}\end{eqnarray}
These quantities quantifies the contribution of intermolecular interactions to
hole (ionization potential, IP) and electron (electron affinity, EA) transport
levels.
Note that different sign convention may apply to  polarization energies.
Here we stick to the one adopted in Ref.~\cite{davino_JCTC15}, where positive 
$\Delta^+$ and $\Delta^-$ determine a reduction of the IP and an increase of the EA, respectively.

An example ME calculation of the hole and electron polarization energies
in the anthracene crystal is provided in \texttt{examples/1.PolEne\_ME}.
The entire example can be run with the script: \texttt{run\_example.sh}.
The calculation consists of the following steps:
\begin{enumerate}
\item ME calculation for the neutral system (spherical cluster).
\item ME calculation for the hole (the central molecule of
  the cluster is a cation)
\item ME calculation for the electron (the central molecule of
  the cluster is an anion)
\item Calculation of polarization energies as total energy differences.~\cite{davino_JCTC15}
\end{enumerate}

The total polarization energy is partitioned into its electrostatic ($\Delta^\pm_E$)
and induction  electrostatic ($\Delta^\pm_I$) contribution:
$\Delta^\pm = \Delta^\pm_E + \Delta^\pm_I $.
Diffeent partitioning of $\Delta^\pm$ into electrostatic and induction terms are possible.
In the present example we adopt the scheme of Ref.~\cite{davino_JCTC15}.



Note that this example calculation employs a relatively small spherical cluster centered
around the molecule of interest.
The result therefore does not apply to the bulk crystal as it is not converged
with respect to the system size.
See Refs.~\cite{davino_JCTC15,Dav16_rev} for discussion on size effects and convergence.





%\section{}

\clearpage
\bibliographystyle{unsrt}
\bibliography{mecr}



\end{document}
